
# Instructions

## Start an R prompt in this directory, then run these commands:
```
install.packages('renv')
renv::activate()
renv::restore()
```

## giveaway_activity.R
Use this script to produce visualizations for tipbot and comment activity on a popular Reddit giveaway thread

